## About

Redmine is a flexible project management web application written using Ruby on Rails framework.

## Features

Some of the main features of Redmine are:
  * Multiple projects support
  * Flexible role based access control
  * Flexible issue tracking system
  * Gantt chart and calendar
  * News, documents & files management
  * Feeds & email notifications
  * Per project wiki
  * Per project forums
  * Time tracking
  * Custom fields for issues, time-entries, projects and users
  * SCM integration (SVN, CVS, Git, Mercurial and Bazaar)
  * Issue creation via email
  * Multiple LDAP authentication support
  * User self-registration support
  * Multilanguage support
  * Multiple databases support

Read more about [Redmine features](https://redmine.org/projects/redmine/wiki/Features).

## Documentation

You can read the [Redmine guide](https://redmine.org/projects/redmine/wiki/Guide).

Other resources:
  * [Changelog](https://redmine.org/projects/redmine/wiki/Changelog)
  * [Frequently Asked Questions](https://redmine.org/projects/redmine/wiki/FAQ)
  * [HowTos](https://redmine.org/projects/redmine/wiki/HowTos)
  * [Plugins](https://redmine.org/projects/redmine/wiki/Plugins)
  * [Themes](https://redmine.org/projects/redmine/wiki/Themes)
  * [Logo and Icon](https://redmine.org/projects/redmine/wiki/Logo)
  * [Third Party Tools](https://redmine.org/projects/redmine/wiki/ThirdPartyTools)

## Support & getting help

For getting help or discussing about Redmine, you can browse the [Redmine forums](http://www.redmine.org/projects/redmine/boards) hosted right here in Redmine. We also have a fairly [active chatroom](https://redmine.org/projects/redmine/wiki/IRC) - join #redmine on the freenode IRC network.

Before submitting a bug report, a patch or a feature request here, please read the [Submission guidelines](https://redmine.org/projects/redmine/wiki/Submissions).
