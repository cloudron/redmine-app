FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && \
    apt-get install -y libmagickwand-dev subversion mercurial darcs cvs bzr && \
    rm -r /var/cache/apt /var/lib/apt/lists

# install rbenv since we need ruby 3.3.4
RUN mkdir -p /usr/local/rbenv && curl -LSs "https://github.com/rbenv/rbenv/archive/refs/tags/v1.3.0.tar.gz" | tar -xz -C /usr/local/rbenv --strip-components 1 -f -
ENV PATH=/usr/local/rbenv/bin:$PATH
ENV RBENV_ROOT=/home/cloudron/rbenv
RUN mkdir -p "$(rbenv root)"/plugins/ruby-build && curl -LSs "https://github.com/rbenv/ruby-build/archive/refs/tags/v20240903.tar.gz" | tar -xz -C "$(rbenv root)"/plugins/ruby-build --strip-components 1 -f -
ARG RUBY_VERSION=3.3.4
RUN rbenv install ${RUBY_VERSION}
ENV PATH=${RBENV_ROOT}/versions/${RUBY_VERSION}/bin:$PATH

# also available from https://redmine.org/releases/redmine-${VERSION}.tar.gz
# renovate: datasource=github-tags depName=redmine/redmine versioning=semver
ARG REDMINE_VERSION=6.0.3
RUN curl -L https://github.com/redmine/redmine/archive/refs/tags/${REDMINE_VERSION}.tar.gz | tar -xz --strip-components 1 -f -

# https://github.com/kontron/redmine_oauth/issues/39
# renovate: datasource=github-releases depName=kontron/redmine_oauth versioning=semver extractVersion=^v(?<version>.+)$
ARG OAUTH_PLUGIN_VERSION=3.0.2
RUN mkdir -p /app/code/plugins/redmine_oauth && \
    curl -L https://github.com/kontron/redmine_oauth/archive/refs/tags/v${OAUTH_PLUGIN_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code/plugins/redmine_oauth  -f -

RUN gem install bundler -v 2.5.23

# copy only to make bundle install pick the right gem for the db adapter
ADD database.yml.template /app/code/config/database.yml
RUN echo "gem 'webrick'" > /app/code/Gemfile.local
RUN bundle config set --local without 'development test' && \
    bundle config set --local path '/run/redmine/vendor' && \
    bundle install && \
    mv /app/code/.bundle /app/code/.bundle.orig && ln -sf /run/redmine/dotbundle /app/code/.bundle && \
    mv /run/redmine/vendor /app/code/vendor.orig && ln -sf /run/redmine/vendor /app/code/vendor

# note the tmp and log directories have to be in run and not tmp because redmine gets upset if they get removed
RUN rm -f /app/code/config/database.yml && ln -s /run/redmine/database.yml /app/code/config/database.yml && \
    rm -f /app/code/config/configuration.yml && ln -s /run/redmine/configuration.yml /app/code/config/configuration.yml && \
    rm -f /app/code/config/secrets.yml && ln -s /app/data/secrets.yml /app/code/config/secrets.yml && \
    rm -rf /app/code/files && ln -s /app/data/files /app/code/files && \
    rm -rf /app/code/public/assets && ln -s /app/data/assets /app/code/public/assets && \
    rm -rf /app/code/public/plugin_assets && ln -s /app/data/plugin_assets /app/code/public/plugin_assets && \
    rm -rf /app/code/themes && ln -s /app/data/themes /app/code/themes && \
    mv /app/code/plugins /app/code/plugins.orig && ln -s /app/data/plugins /app/code/plugins && \
    rm -rf /app/code/tmp && ln -s /run/redmine/tmp /app/code/tmp && \
    rm -rf /app/code/log && ln -s /run/redmine/log /app/code/log

# bundle exec can modify Gemfile.lock (https://github.com/bundler/bundler/issues/5245)
RUN mv /app/code/Gemfile.lock /app/code/Gemfile.lock.save && ln -s /run/redmine/Gemfile.lock /app/code/Gemfile.lock

# hack to make schema.rb writable by active record
RUN ln -sf /run/redmine/schema.rb /app/code/db/schema.rb

# this allows user to create ssh keys for the redmine user for clone of private repos
RUN ln -s /app/data/.ssh /home/cloudron/.ssh

# These variables are set here so that 'cloudron exec' will pick them up
ENV RAILS_ENV=production
ENV REDMINE_LANG=en
# sass theme building requires this
ENV LANG=C.UTF-8

# Allow to override settings
RUN rm -rf /app/code/config/additional_environment.rb && ln -s /app/data/additional_environment.rb /app/code/config/additional_environment.rb

# allow imagemagik to generate PDF thumbnails (https://forum.cloudron.io/topic/6736/imagemagick-memory-allocation-failed)
COPY policy.xml /etc/ImageMagick-6/policy.xml

COPY production.rb /app/code/config/environments/production.rb
COPY start.sh database.yml.template configuration.yml.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
