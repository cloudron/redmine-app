#!/bin/bash

set -eu -o pipefail

echo "=> Ensure directories"
mkdir -p /app/data/{files,assets,plugin_assets,themes,redmine_extensions,.ssh}
mkdir -p /run/redmine/{log,/tmp/pdf,vendor,dotbundle}
rm -f /app/code/tmp/pids/server.pid

echo -e "[client]\npassword=${CLOUDRON_MYSQL_PASSWORD}" > /run/redmine/mysql-extra
readonly mysql="mysql --defaults-file=/run/redmine/mysql-extra --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} -P ${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE} "

export HOME=/tmp

[[ ! -d /app/data/plugins ]] && cp -r /app/code/plugins.orig /app/data/plugins

cp /app/code/Gemfile.lock.save /run/redmine/Gemfile.lock
touch /run/redmine/schema.rb
touch /app/data/additional_environment.rb

echo "=> Generate database config"
cp /app/pkg/database.yml.template /run/redmine/database.yml
yq eval -i ".production.database=\"${CLOUDRON_MYSQL_DATABASE}\"" /run/redmine/database.yml
yq eval -i ".production.host=\"${CLOUDRON_MYSQL_HOST}\"" /run/redmine/database.yml
yq eval -i ".production.port=\"${CLOUDRON_MYSQL_PORT}\"" /run/redmine/database.yml
yq eval -i ".production.username=\"${CLOUDRON_MYSQL_USERNAME}\"" /run/redmine/database.yml
yq eval -i ".production.password=\"${CLOUDRON_MYSQL_PASSWORD}\"" /run/redmine/database.yml

echo "=> Generate email config"
cp /app/pkg/configuration.yml.template /run/redmine/configuration.yml
yq eval -i ".default.email_delivery.smtp_settings.address=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" /run/redmine/configuration.yml
yq eval -i ".default.email_delivery.smtp_settings.port=${CLOUDRON_MAIL_SMTP_PORT}" /run/redmine/configuration.yml
yq eval -i ".default.email_delivery.smtp_settings.domain=\"${CLOUDRON_MAIL_DOMAIN}\"" /run/redmine/configuration.yml
yq eval -i ".default.email_delivery.smtp_settings.user_name=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" /run/redmine/configuration.yml
yq eval -i ".default.email_delivery.smtp_settings.password=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" /run/redmine/configuration.yml

echo "=> Fixing /tmp permissions"
# https://blog.diacode.com/fixing-temporary-dir-problems-with-ruby-2
chmod o+t /tmp

# The .done flag is not ideal since it doesn't know if user added/removed a gem behind our back
echo "=> Installing plugin gems"
cp -r /app/code/.bundle.orig/* /run/redmine/dotbundle/
if [[ ! -f /run/redmine/vendor/.done ]]; then
    echo "=> Copying redmine vendor gems on first run"
    cp -r /app/code/vendor.orig/* /run/redmine/vendor
    echo "=> Installing gems of plugins"
    bundle install
fi
touch /run/redmine/vendor/.done

chown -R cloudron:cloudron /app/data /run/redmine

if [[ ! -f /app/data/secrets.yml ]]; then
    echo "=> First run"

    echo "=> Generate session secret"
    secret=$(bundle exec rails secret 2>/dev/null)
    cat > /app/data/secrets.yml <<EOF
production:
  secret_key_base: ${secret}
EOF
    export SECRET_KEY_BASE=$(cat /app/data/secrets.yml | grep 'secret_key_base:' | sed -e "s/.*secret_key_base:\s*//")
    echo "=> Run database migration"
    gosu cloudron bundle exec rake db:migrate

    echo "=> Setup default data"
    gosu cloudron bundle exec rake redmine:load_default_data

    # disable registration with oidc
    [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]] && $mysql -e "INSERT INTO settings (name, value, updated_on) VALUES ('self_registration', 0, NOW())"
else
    echo "=> Run database migration"
    export SECRET_KEY_BASE=$(cat /app/data/secrets.yml | grep 'secret_key_base:' | sed -e "s/.*secret_key_base:\s*//")
    gosu cloudron bundle exec rake db:migrate
fi

echo "=> Ensure mail from address"
mail_from="${CLOUDRON_MAIL_FROM}"
# https://www.redmine.org/issues/5913 . Setting display name will override author name
[[ -n "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-}" ]] && mail_from="${CLOUDRON_MAIL_FROM_DISPLAY_NAME} <${CLOUDRON_MAIL_FROM}>"
# single quote support in display name
mail_from_base64=$(echo -n "${mail_from}" | base64)
$mysql -e "INSERT INTO settings (name, value) VALUES ('mail_from', FROM_BASE64('${mail_from_base64}')) ON DUPLICATE KEY UPDATE name='mail_from', value=FROM_BASE64('${mail_from_base64}');"

echo "=> Set hostname"
$mysql -e "INSERT INTO settings (name, value) VALUES ('host_name', '${CLOUDRON_APP_DOMAIN}') ON DUPLICATE KEY UPDATE name='host_name', value='${CLOUDRON_APP_DOMAIN}';"
$mysql -e "INSERT INTO settings (name, value) VALUES ('protocol', 'https') ON DUPLICATE KEY UPDATE name='protocol', value='https';"

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "=> Update OIDC config"

    # update oidc plugin files
    rm -rf /app/data/plugins/redmine_oauth && cp -R /app/code/plugins.orig/redmine_oauth /app/data/plugins/redmine_oauth

    provider_name=$(php -r "echo addslashes(preg_replace('/[\xf0-\xf7].../s', '', \"${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}\"));")

    # settings.name has no unique constraint! so, we cannot duplicate key update
    $mysql -e "DELETE FROM settings WHERE name='plugin_redmine_oauth';"
    $mysql -e "INSERT INTO settings (name, value, updated_on) VALUES ( \
            'plugin_redmine_oauth', \
            '--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\noauth_name: Custom\nbutton_color: \"#ffbe6f\"\nbutton_icon: fas fa-address-card\nsite: \'\'\nclient_id: ${CLOUDRON_OIDC_CLIENT_ID}\nclient_secret: ${CLOUDRON_OIDC_CLIENT_SECRET}\ntenant_id: \'\'\ncustom_name: \"${provider_name}\"\ncustom_auth_endpoint: ${CLOUDRON_OIDC_AUTH_ENDPOINT}\ncustom_token_endpoint: ${CLOUDRON_OIDC_TOKEN_ENDPOINT}\ncustom_profile_endpoint: ${CLOUDRON_OIDC_PROFILE_ENDPOINT}\ncustom_scope: openid profile email\ncustom_uid_field: sub\ncustom_email_field: email\nself_registration: \"3\"\n', \
            NOW() )"
fi

echo "=> Migrate plugins"
gosu cloudron bundle exec rake redmine:plugins:migrate

echo "=> Precompile assets"
gosu cloudron bundle exec rake assets:precompile

echo "==> Starting redmine"
exec /usr/local/bin/gosu cloudron:cloudron bundle exec rails server -u webrick -e production -b 0.0.0.0

