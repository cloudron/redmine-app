This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: admin<br/>

<sso>
Cloudron users can login after their accounts approved by admin.
You can make user accounts approved automatically by updating `Self-registration` setting on `Administration -> Authentication` tab.
</sso>
