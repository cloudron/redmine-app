#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const adminUser = 'admin';
    const adminInitialPassword = 'admin';
    const adminPassword = 'cloudrondoesthejob';
    const PROJECT_NAME = 'testproject';

    let browser, app, cloudronName;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) {
            cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
            // get rid of mb4 characters
            cloudronName = cloudronName.replace(/[\uD800-\uDBFF][\uDC00-\uDFFF]|[\u{10000}-\u{10FFFF}]/gu, '');
        }
    }

    async function login(user=username, pass=password) {
        await browser.get(`https://${app.fqdn}/login`);
        await browser.findElement(By.xpath('//input[@id="username"]')).sendKeys(user);
        await browser.findElement(By.xpath('//input[@id="password"]')).sendKeys(pass);
        await browser.findElement(By.xpath('//input[@id="login-submit"]')).click();
        await waitForElement(By.xpath('//a[@class="my-account"]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(4000);

        await waitForElement(By.xpath('//button[@id="login-oauth-submit" and (contains(., "Cloudron") or contains(., "${cloudronName}"))]'));
        await browser.findElement(By.xpath('//button[@id="login-oauth-submit" and (contains(., "Cloudron") or contains(., "${cloudronName}"))]')).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//a[@class="my-account"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//a[@class="logout"]'));
        await browser.findElement(By.xpath('//a[@class="logout"]')).click();
        await waitForElement(By.xpath('//a[@class="login"]'));
    }

    async function adminPasswordChange() {
        await browser.get(`https://${app.fqdn}/my/password`);
        await browser.findElement(By.xpath('//input[@id="password"]')).sendKeys(adminInitialPassword);
        await browser.findElement(By.xpath('//input[@id="new_password"]')).sendKeys(adminPassword);
        await browser.findElement(By.xpath('//input[@id="new_password_confirmation"]')).sendKeys(adminPassword);
        await browser.findElement(By.xpath('//input[@type="submit"]')).click();
        await waitForElement(By.xpath('//div[@id="flash_notice"]'));
    }

    async function activateUsersAutomatically() {
        await browser.get(`https://${app.fqdn}/settings?tab=authentication`);
        await browser.sleep(2000);
        await browser.findElement(By.id('settings_self_registration')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//select[@id="settings_self_registration"]//option[@value="3"]')).click();
        await browser.findElement(By.xpath('//label[@for="settings_self_registration"]')).click();

        await browser.sleep(2000);
        const button = browser.findElement(By.xpath('//div[@id="tab-content-authentication"]//input[@type="submit" and @value="Save"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await button.click();
        await waitForElement(By.xpath('//div[@id="flash_notice" and contains(., "Successful update")]'));
    }

    async function createProject() {
        await browser.get(`https://${app.fqdn}/projects/new`);
        await browser.findElement(By.xpath('//input[@id="project_name"]')).sendKeys(PROJECT_NAME);
        await browser.findElement(By.xpath('//input[@type="submit" and @name="commit"]')).click();
        await waitForElement(By.xpath('//div[contains(text(), "Successful creation.")]'));
    }

    async function projectExists() {
        await browser.get(`https://${app.fqdn}/projects/`);
        await waitForElement(By.xpath(`//a[contains(@class, "project") and text()="${PROJECT_NAME}"]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('install app (No SSO)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login as admin', login.bind(null, adminUser, adminInitialPassword));
    it('can change admin password', adminPasswordChange);
    it('can create project', createProject);
    it('project exists', projectExists);
    it('can logout', logout);

    it('uninstall app (No SSO)', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // SSO
    it('install app (SSO)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login as admin', login.bind(null, adminUser, adminInitialPassword));
    it('can change admin password', adminPasswordChange);

    xit('can activate users automatically', activateUsersAutomatically);
    it('can create project', createProject);
    it('project exists', projectExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, username, password, false));
    it('can logout', logout);


    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('project exists', projectExists);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('project exists', projectExists);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('project exists', projectExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync('cloudron install --appstore-id org.redmine.coudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login as admin', login.bind(null, adminUser, adminInitialPassword));
    it('can change admin password', adminPasswordChange);
    it('can create project', createProject);
    it('project exists', projectExists);
    it('can actiate users automatically', activateUsersAutomatically);
    it('can logout', logout);

    it('can get app information', getAppInfo);
    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });

    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('project exists', projectExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
   });
});
