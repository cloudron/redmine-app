# Redmine Cloudron App

This repository contains the Cloudron app package source for [Redmine](http://www.redmine.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.redmine.coudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.redmine.coudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd redmine-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the data is still there.

```
cd redmine-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Notes

Per https://www.redmine.org/projects/redmine/wiki/RedmineInstall#Alternative-to-manual-installation 

```
Note: Webrick is not suitable for production use, please only use webrick for testing that the installation up to this point is functional. Use one of the many other guides in this wiki to setup redmine to use either Passenger (aka mod_rails), FCGI or a Rack server (Unicorn, Thin, Puma, hellip;) to serve up your redmine.
```

https://github.com/junxy/running-redmine-on-puma/blob/master/doc/installation.md has a puma installation - https://raw.githubusercontent.com/junxy/running-redmine-on-puma/master/config/redmine/puma.rb.example

